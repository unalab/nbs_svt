$(document).ready(function() {

    /*materialize inizialize*/
    $('select').formSelect();
    $(".dropdown-trigger").dropdown();

    $("#chooseScenario").change(function() {
		manageIframeDisplay();
	});
    manageIframeDisplay();
	 
});


function manageIframeDisplay() {
		  
    if ($("#chooseScenario").val() == "1") {
            $("#scenario1").css('display','block');
            $("#scenario2").css('display','none');
            $("#scenario3").css('display','none');
            $("#scenario4").css('display','none');
            $("#scenario5").css('display','none');
            
            
    }else if ($("#chooseScenario").val() == "2") {
            $("#scenario1").css('display','none');
            $("#scenario2").css('display','block');
            $("#scenario3").css('display','none');
            $("#scenario4").css('display','none');
            $("#scenario5").css('display','none');
            
            
    }else if ($("#chooseScenario").val() == "3") {
            $("#scenario1").css('display','none');
            $("#scenario2").css('display','none');
            $("#scenario3").css('display','block');
            $("#scenario4").css('display','none');
            $("#scenario5").css('display','none');
            
            
    }else if ($("#chooseScenario").val() == "4") {
            $("#scenario1").css('display','none');
            $("#scenario2").css('display','none');
            $("#scenario3").css('display','none');
            $("#scenario4").css('display','block');
            $("#scenario5").css('display','none');
            
            
    }else if ($("#chooseScenario").val() == "5") {
            $("#scenario1").css('display','none');
            $("#scenario2").css('display','none');
            $("#scenario3").css('display','none');
            $("#scenario4").css('display','none');
            $("#scenario5").css('display','block');
            
            
    }else if ($("#chooseScenario").val() == "15") {
            $("#scenario1").css('display','none');
            $("#scenario2").css('display','none');
            $("#scenario3").css('display','none');
            $("#scenario4").css('display','none');
            $("#scenario5").css('display','none');
            
                            
    }
}


Sbi.sdk.services.setBaseUrl({
    protocol: 'http'
    , host: '217.172.12.202'
    , port: '8089'
    , contextPath: 'knowage'
    , controllerPath: 'servlet/AdapterHTTP'
});

var cb = function(result, args, success) {

    if(success === true) {
     this.execTest1();
     this.execTest2();
     this.execTest2();
     this.execTest3();
     this.execTest4();
     this.execTest5();
     
    } else {
        alert('ERROR: Wrong username or password');
    }
};


Sbi.sdk.api.authenticate({
    params: {
        user: 'biadmin'
        , password: 'biadmin'
    }

    , callback: {
        fn: cb
        , scope: this
        //, args: {arg1: 'A', arg2: 'B', ...}
    }
});



// Sbi.sdk.api.authenticate('biadmin','biadmin', cb, this);





// TEST 1: getDocumentUrl -----------------------------------------------------
this.execTest1 = function() {
    var url = Sbi.sdk.api.getDocumentUrl({
        documentLabel: 'Eindhoven1'
        , executionRole: '/spagobi/user'
        , displayToolbar: false
        , displaySliders: false
        , height: '500px'
        , width: '800px'
        , iframe: {
            style: 'border: 0px;'
        }
    });
    document.getElementById('execiframe').src = url;
};


this.execTest2 = function() {
    var url = Sbi.sdk.api.getDocumentUrl({
        documentLabel: 'Eindhoven2'
        , executionRole: '/spagobi/user'
        , displayToolbar: false
        , displaySliders: false
        , height: '500px'
        , width: '800px'
        , iframe: {
            style: 'border: 0px;'
        }
    });
    document.getElementById('execiframe2').src = url;
};


this.execTest3 = function() {
    var url = Sbi.sdk.api.getDocumentUrl({
        documentLabel: 'Eindhoven3'
        , executionRole: '/spagobi/user'
        , displayToolbar: false
        , displaySliders: false
        , height: '500px'
        , width: '800px'
        , iframe: {
            style: 'border: 0px;'
        }
    });
    document.getElementById('execiframe3').src = url;
};


this.execTest4 = function() {
    var url = Sbi.sdk.api.getDocumentUrl({
        documentLabel: 'Eindhoven4'
        , executionRole: '/spagobi/user'
        , displayToolbar: false
        , displaySliders: false
        , height: '500px'
        , width: '800px'
        , iframe: {
            style: 'border: 0px;'
        }
    });
    document.getElementById('execiframe4').src = url;
};


this.execTest5 = function() {
    var url = Sbi.sdk.api.getDocumentUrl({
        documentLabel: 'Eindhoven5'
        , executionRole: '/spagobi/user'
        , displayToolbar: false
        , displaySliders: false
        , height: '500px'
        , width: '800px'
        , iframe: {
            style: 'border: 0px;'
        }
    });
    document.getElementById('execiframe5').src = url;
};

